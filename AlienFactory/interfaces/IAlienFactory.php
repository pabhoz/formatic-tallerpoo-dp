<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pabhoz
 */
interface IAlienFactory {
    
    public static function getJupiterAlien($nombre,$edad,$especie): JupiterAlien;
    public static function getMarsAlien($nombre,$edad,$especie): MarsAlien;
    public static function getMoonAlien($nombre,$edad,$especie): MoonAlien;
    public static function getPlutoAlien($nombre,$edad,$especie): PlutoAlien;
    public static function getSaturnAlien($nombre,$edad,$especie): SaturnAlien;
    public static function getVenusAlien($nombre,$edad,$especie): VenusAlien;
}
