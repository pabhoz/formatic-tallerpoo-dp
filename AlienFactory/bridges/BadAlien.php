<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BadAlien
 *
 * @author pabhoz
 */
class BadAlien extends Alien implements IBadAlien{
    
    public function __construct($nombre, $edad, $especie, $planeta) {
        parent::__construct($nombre, $edad, $especie, $planeta);
        $this->setMoral("Malo");
    }
    
    public function interact() {
        return BadAlien::COMUNICACION." dice: Hola terricola, rindanse ante la "
                . "invasión de ".$this->getNombre();
    }

    public function destruirPlaneta(Planet $planeta) {
        $planeta->setEstado("destruido");
        return $planeta->getEstado();
    }

    public function pedirRefuerzos() {
        return "Solicitando refuerzos Muahahaha";
    }

}
